package test;

import static org.junit.Assert.*;

import org.junit.Test;
import model.Line;

public class LineTest {

	@Test
	public void limitTest() {
		Line<Boolean> line = new Line<Boolean>(1);
		Line<Boolean> line2 = new Line<Boolean>(3);
		line.addElement(true);
		line2.addElement(true);
		assertTrue(line.getLimit() == 0 && (line2.getLimit() == 2));
	}

	@Test
	public void checkAllElementEqualsTest() {		
		Line<Boolean> line = new Line<Boolean>(3);
		line.addElement(true);
		line.addElement(true);		
		assertTrue(line.addElement(true));
	}

	@Test
	public void checkAllElementNotEqualsTest() {
		Line<Boolean> line = new Line<Boolean>(5);
		line.addElement(true);
		line.addElement(true);
		line.addElement(true);
		line.addElement(false);
		assertFalse(line.addElement(true));
	}

	@Test
	public void insertElement() {
		Line<Boolean> line = new Line<Boolean>(3);
		line.addElement(true);
		line.addElement(true);
		assertTrue(line.getLastElement());
	}

}
