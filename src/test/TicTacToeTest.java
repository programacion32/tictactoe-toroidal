package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.TicTacToe;

public class TicTacToeTest {

	@Test
	public void winnerTest() {
		TicTacToe game = new TicTacToe("X","O");
		game.insertPlay(8);// O
		game.insertPlay(3);// X
		game.insertPlay(2);// O
		game.insertPlay(4);// X
		game.insertPlay(5);// O
		assertEquals(game.getWinner(), "X");
	}

	@Test
	public void turnCounterTest() {
		TicTacToe game = new TicTacToe("X","O");		
		game.insertPlay(1);
		game.insertPlay(2);
		game.insertPlay(3);

		assertTrue(game.getTurnCounter() == 3);
	}

	public void turnTest() {
		TicTacToe game = new TicTacToe();
		game.isTurn();
	}

}
