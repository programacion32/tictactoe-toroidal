package view;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.SwingConstants;

import model.TicTacToe;
import java.awt.Font;

import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OptionView {

	private JFrame frame;
	private TicTacToe game;
	private MainView mainView;
	private JLayeredPane backGroundLayer;
	private JTextField textPlayer1Name;
	private JTextField textPlayer2Name;

	/**
	 * Create the application.
	 */
	public OptionView(MainView mainView) {
		this.mainView = mainView;
		this.game = this.mainView.getGame();
		initialize();
	}

	private void toMainView() {
		game.play();
		this.game.getPlayer1().setName(textPlayer1Name.getText());
		this.game.getPlayer2().setName(textPlayer2Name.getText());
		frame.setVisible(false);
		mainView.getFrame().setBounds(this.frame.getBounds());
		mainView.getFrame().setVisible(true);
	}

	private void setCheck() {
		this.game.getPlayer1().setName(textPlayer1Name.getText());
		this.game.getPlayer2().setName(textPlayer2Name.getText());
		this.game.setNormalGame(!this.game.isNormalGame());
		frame.setVisible(false);
		this.mainView.getFrame().setBounds(this.frame.getBounds());
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		this.frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle(this.mainView.getFrame().getTitle());
		frame.setBounds(this.mainView.getFrame().getBounds());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		optionsView();
	}

	private void optionsView() {
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		this.backGroundLayer = new JLayeredPane();
		backGroundLayer.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(backGroundLayer);
		backGroundLayer.setLayout(null);

		JLabel lblBackGroundImg = new JLabel("");
		lblBackGroundImg.setBounds(0, 0, 800, 600);
		lblBackGroundImg.setBackground(Color.black);
		lblBackGroundImg.setHorizontalAlignment(SwingConstants.CENTER);
		lblBackGroundImg.setIcon(new ImageIcon(MainView.class.getResource(this.mainView.backGroundImg)));
		backGroundLayer.add(lblBackGroundImg);

		JLabel lblNormalGame = new JLabel("Normal Game");
		lblNormalGame.setForeground(Color.blue);
		backGroundLayer.setLayer(lblNormalGame, 1);
		lblNormalGame.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 40));
		lblNormalGame.setHorizontalAlignment(SwingConstants.CENTER);
		lblNormalGame.setBounds(204, 166, 279, 57);
		backGroundLayer.add(lblNormalGame);

		JLabel lblNormalGameCheck = new JLabel("");		
		if (this.game.isNormalGame()) {
			lblNormalGameCheck.setIcon(new ImageIcon(OptionView.class.getResource("/Images/CheckBoxON.png")));
		} else {
			lblNormalGameCheck.setIcon(new ImageIcon(OptionView.class.getResource("/Images/CheckBoxOFF.png")));
		}

		backGroundLayer.setLayer(lblNormalGameCheck, 1);
		lblNormalGameCheck.setHorizontalAlignment(SwingConstants.CENTER);
		lblNormalGameCheck.setBounds(510, 166, 60, 60);
		backGroundLayer.add(lblNormalGameCheck);

		lblNormalGameCheck.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				setCheck();
			}
		});

		JLabel lblPlayer1Name = new JLabel("Player 1");
		lblPlayer1Name.setForeground(Color.blue);
		backGroundLayer.setLayer(lblPlayer1Name, 1);
		lblPlayer1Name.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlayer1Name.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 40));
		lblPlayer1Name.setBounds(204, 246, 279, 57);
		backGroundLayer.add(lblPlayer1Name);

		textPlayer1Name = new JTextField();
		backGroundLayer.setLayer(textPlayer1Name, 1);
		textPlayer1Name.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 40));
		textPlayer1Name.setBackground(new Color(0, true));
		textPlayer1Name.setBorder(null);
		textPlayer1Name.setForeground(Color.blue);
		textPlayer1Name.setText(this.game.getPlayer1().getName());
		textPlayer1Name.setHorizontalAlignment(SwingConstants.CENTER);
		textPlayer1Name.setBounds(461, 246, 156, 50);
		backGroundLayer.add(textPlayer1Name);
		textPlayer1Name.setColumns(10);

		JLabel lblPlayer2Name = new JLabel("Player 2");
		lblPlayer2Name.setForeground(Color.blue);
		backGroundLayer.setLayer(lblPlayer2Name, 1);
		lblPlayer2Name.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlayer2Name.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 40));
		lblPlayer2Name.setBounds(204, 314, 279, 57);
		backGroundLayer.add(lblPlayer2Name);

		textPlayer2Name = new JTextField();
		backGroundLayer.setLayer(textPlayer2Name, 1);
		textPlayer2Name.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 40));
		textPlayer2Name.setBackground(new Color(0, true));
		textPlayer2Name.setBorder(null);
		textPlayer2Name.setForeground(Color.blue);
		textPlayer2Name.setText(this.game.getPlayer2().getName());
		textPlayer2Name.setHorizontalAlignment(SwingConstants.CENTER);
		textPlayer2Name.setBounds(461, 314, 156, 50);
		backGroundLayer.add(textPlayer2Name);
		textPlayer2Name.setColumns(10);

		JLabel lblOk = new JLabel("OK");
		lblOk.setForeground(Color.blue);
		backGroundLayer.setLayer(lblOk, 1);
		lblOk.setHorizontalAlignment(SwingConstants.CENTER);
		lblOk.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 40));
		lblOk.setBounds(240, 408, 338, 50);
		backGroundLayer.add(lblOk);

		lblOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				toMainView();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lblOk.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblOk.setForeground(new Color(0, 0, 255));
			}
		});
		
		JLabel lblAuthor = new JLabel("Alpuin Matias");
		backGroundLayer.setLayer(lblAuthor, 1);
		lblAuthor.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 25));
		lblAuthor.setHorizontalAlignment(SwingConstants.CENTER);
		lblAuthor.setForeground(Color.BLUE);
		lblAuthor.setBounds(598, 540, 202, 60);
		backGroundLayer.add(lblAuthor);
	}

}
