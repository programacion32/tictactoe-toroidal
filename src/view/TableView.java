package view;

import java.awt.Color;

import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.SwingConstants;

import model.TicTacToe;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class TableView {

	private JFrame frame;
	private TicTacToe game;
	private MainView mainView;
	private String tableroImg;
	private JLabel lblWinner;
	private JLabel lblTurn;
	private JLabel lblMoves;

	/**
	 * Create the application.
	 */
	public TableView(MainView mainView) {
		this.mainView = mainView;
		this.game = this.mainView.getGame();
		this.tableroImg = "/Images/Tablero 01 - Glow.png";
		lblMoves = new JLabel(Integer.toString(this.game.getTurnCounter()));
		lblWinner = new JLabel(this.game.getWinner());
		lblTurn = new JLabel(game.whoPlaysNow().getName());
		initialize();
	}

	private void insertPlay(int pos, JLabel lbl) {
		String resource = this.game.whoPlaysNow().getImage();
		if (game.insertPlay(pos)) {
			lbl.setIcon(new ImageIcon(TableView.class.getResource(resource)));
		}
		lblMoves.setText(Integer.toString(this.game.getTurnCounter()));
		lblWinner.setText(this.game.getWinner());
		lblTurn.setText(this.game.whoPlaysNow().getName());
		lblWinner.setVisible(this.game.isGameOver());
	}

	private void toMainView() {
		game.play();
		frame.setVisible(false);
		mainView.getFrame().setBounds(this.frame.getBounds());
		mainView.getFrame().setVisible(true);
	}

	private void restart() {
		game.play();
		lblMoves = new JLabel(Integer.toString(this.game.getTurnCounter()));
		lblWinner = new JLabel(this.game.getWinner());
		lblTurn = new JLabel(this.game.whoPlaysNow().getName());
		frame.setVisible(false);
		mainView.getFrame().setBounds(this.frame.getBounds());
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setTitle(this.mainView.getFrame().getTitle());
		frame.setBounds(this.mainView.getFrame().getBounds());		

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLayeredPane backGroundLayer = new JLayeredPane();
		backGroundLayer.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(backGroundLayer);
		backGroundLayer.setLayout(null);

		JLabel lblBackGroundImg = new JLabel("");
		lblBackGroundImg.setBounds(0, 0, 800, 600);
		lblBackGroundImg.setBackground(Color.black);
		lblBackGroundImg.setHorizontalAlignment(SwingConstants.CENTER);
		lblBackGroundImg.setIcon(new ImageIcon(MainView.class.getResource(this.mainView.backGroundImg)));
		backGroundLayer.add(lblBackGroundImg);

		JLabel lblTableImg = new JLabel("");
		backGroundLayer.setLayer(lblTableImg, 1);
		lblTableImg.setIcon(new ImageIcon(TableView.class.getResource(this.tableroImg)));
		lblTableImg.setHorizontalAlignment(SwingConstants.CENTER);
		lblTableImg.setBackground(Color.black);
		lblTableImg.setBounds(0, 0, 800, 600);
		backGroundLayer.add(lblTableImg);

		JLabel btnMainMenu = new JLabel("MAIN MENU");
		btnMainMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				toMainView();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				btnMainMenu.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				btnMainMenu.setForeground(Color.blue);
			}
		});

		btnMainMenu.setForeground(Color.blue);
		btnMainMenu.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 20));
		btnMainMenu.setHorizontalAlignment(SwingConstants.CENTER);

		backGroundLayer.setLayer(btnMainMenu, 1);
		btnMainMenu.setBounds(678, 325, 101, 47);
		backGroundLayer.add(btnMainMenu);

		JLabel btnRestart = new JLabel("RESTART");
		btnRestart.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				restart();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				btnRestart.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				btnRestart.setForeground(Color.blue);
			}
		});
		btnRestart.setForeground(Color.blue);
		btnRestart.setFont(this.mainView.customFont.deriveFont(Font.BOLD, 20));
		btnRestart.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(btnRestart, 1);
		btnRestart.setBounds(678, 232, 101, 47);
		backGroundLayer.add(btnRestart);

		JLabel lblTurnf = new JLabel("Turn: ");
		backGroundLayer.setLayer(lblTurnf, 1);
		lblTurnf.setForeground(Color.blue);
		lblTurnf.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 40));
		lblTurnf.setBounds(10, 11, 110, 47);
		backGroundLayer.add(lblTurnf);

		backGroundLayer.setLayer(lblTurn, 1);
		lblTurn.setForeground(Color.red);
		lblTurn.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 40));
		lblTurn.setHorizontalAlignment(SwingConstants.LEFT);
		lblTurn.setBounds(114, 11, 276, 47);
		backGroundLayer.add(lblTurn);

		JLabel lblMovesf = new JLabel("Moves:");
		backGroundLayer.setLayer(lblMovesf, 1);
		lblMovesf.setHorizontalAlignment(SwingConstants.LEFT);
		lblMovesf.setForeground(Color.blue);
		lblMovesf.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 40));
		lblMovesf.setBounds(617, 11, 130, 47);
		backGroundLayer.add(lblMovesf);

		backGroundLayer.setLayer(lblMoves, 1);
		lblMoves.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoves.setForeground(Color.red);
		lblMoves.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 40));
		lblMoves.setBounds(739, 11, 40, 47);
		backGroundLayer.add(lblMoves);

		backGroundLayer.setLayer(lblWinner, 1);
		lblWinner.setHorizontalAlignment(SwingConstants.CENTER);
		lblWinner.setForeground(Color.red);
		lblWinner.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 50));
		lblWinner.setBounds(0, 0, 800, 255);
		lblWinner.setVisible(false);
		backGroundLayer.add(lblWinner);
		
		JLabel lblAuthor = new JLabel("Alpuin Matias");
		backGroundLayer.setLayer(lblAuthor, 1);
		lblAuthor.setFont(this.mainView.customFont.deriveFont(Font.BOLD | Font.ITALIC, 25));
		lblAuthor.setHorizontalAlignment(SwingConstants.CENTER);
		lblAuthor.setForeground(Color.BLUE);
		lblAuthor.setBounds(598, 540, 202, 60);
		backGroundLayer.add(lblAuthor);

		// --- Button table Area ---

		JLabel lblC1 = new JLabel("");
		lblC1.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC1, 1);
		lblC1.setBounds(246, 168, 101, 87);
		backGroundLayer.add(lblC1);

		JLabel lblC2 = new JLabel("");
		lblC2.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC2, 1);
		lblC2.setBounds(349, 168, 101, 87);
		backGroundLayer.add(lblC2);

		JLabel lblC3 = new JLabel("");
		lblC3.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC3, 1);
		lblC3.setBounds(453, 168, 101, 87);
		backGroundLayer.add(lblC3);

		JLabel lblC4 = new JLabel("");
		lblC4.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC4, 1);
		lblC4.setBounds(246, 259, 101, 87);
		backGroundLayer.add(lblC4);

		JLabel lblC5 = new JLabel("");
		lblC5.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC5, 1);
		lblC5.setBounds(349, 259, 101, 87);
		backGroundLayer.add(lblC5);

		JLabel lblC6 = new JLabel("");
		lblC6.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC6, 1);
		lblC6.setBounds(453, 259, 101, 87);
		backGroundLayer.add(lblC6);

		JLabel lblC7 = new JLabel("");
		lblC7.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC7, 1);
		lblC7.setBounds(246, 350, 101, 87);
		backGroundLayer.add(lblC7);

		JLabel lblC8 = new JLabel("");
		lblC8.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC8, 1);
		lblC8.setBounds(349, 350, 101, 87);
		backGroundLayer.add(lblC8);

		JLabel lblC9 = new JLabel("");
		lblC9.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(lblC9, 1);
		lblC9.setBounds(453, 350, 101, 87);
		backGroundLayer.add(lblC9);

		// --- mousePressed table Area ---

		lblC1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(1, lblC1);
			}
		});

		lblC2.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(2, lblC2);
			}
		});

		lblC3.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(3, lblC3);
			}
		});

		lblC4.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(4, lblC4);
			}
		});

		lblC5.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(5, lblC5);
			}
		});

		lblC6.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(6, lblC6);
			}
		});

		lblC7.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(7, lblC7);
			}
		});

		lblC8.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(8, lblC8);
			}
		});

		lblC9.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				insertPlay(9, lblC9);
			}
		});

	}
}
