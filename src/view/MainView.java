package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;

import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.SwingConstants;

import model.TicTacToe;

import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.awt.CardLayout;

public class MainView {

	private JFrame frame;
	private TicTacToe game;
	private String circleImg;
	private String crossImg;
	
	public String backGroundImg;
	public InputStream fontSource = MainView.class.getResourceAsStream("/fonts/Marske.ttf");
	public Font customFont;

	public MainView() {

		this.circleImg = "/images/Circle Glow 01 82x82.png";
		this.crossImg = "/images/Cross Glow 01 82x82.png";
		this.backGroundImg = "/images/Esfera 01 800x600.gif";

		try {
			this.customFont = Font.createFont(Font.TRUETYPE_FONT, fontSource);
		} catch (FontFormatException e) {
			System.out.println("Error Formato fuente:" + e);
			this.customFont = new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 35);
		} catch (IOException e) {
			System.out.println("Error archivo Font no encontrado:" + e);
			this.customFont = new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 35);
		}

		newGame();
		initialize();
		frame.setVisible(true);
	}

	public void setGame(TicTacToe game) {
		this.game = game;
	}

	public JFrame getFrame() {
		return frame;
	}

	public TicTacToe getGame() {
		return this.game;
	}

	private void newGame() {
		this.game = new TicTacToe();
		this.game.setNormalGame(false);
		this.game.play();
		this.game.getPlayer1().setImage(circleImg);
		this.game.getPlayer2().setImage(crossImg);
	}

	private void toTableroView() {
		frame.setVisible(false);
		TableView tableroView = new TableView(this);
	}

	private void toOptionView() {
		frame.setVisible(false);
		OptionView optionView = new OptionView(this);
	}

	private void initialize() {

		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("TicTacToe");
		frame.setBounds(0, 0, 806, 629);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));

		mainViewDisplay();
	}

	private void mainViewDisplay() {

		JLayeredPane backGroundLayer = new JLayeredPane();
		frame.getContentPane().add(backGroundLayer, "backGroundLayer");
		backGroundLayer.setLayout(null);

		JLabel lblBackGroundImg = new JLabel("");
		lblBackGroundImg.setBounds(0, 0, 800, 600);
		lblBackGroundImg.setBackground(Color.black);
		lblBackGroundImg.setHorizontalAlignment(SwingConstants.CENTER);
		lblBackGroundImg.setIcon(new ImageIcon(MainView.class.getResource(this.backGroundImg)));
		backGroundLayer.add(lblBackGroundImg);

		JLabel btnNewGame = new JLabel("New Game");
		btnNewGame.setForeground(Color.blue);
		btnNewGame.setFont(customFont.deriveFont(Font.BOLD, 35));
		btnNewGame.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(btnNewGame, 1);
		btnNewGame.setBounds(10, 227, 780, 47);
		backGroundLayer.add(btnNewGame);

		btnNewGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				toTableroView();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewGame.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				btnNewGame.setForeground(Color.blue);
			}
		});

		JLabel btnOptions = new JLabel("Options");
		btnOptions.setForeground(new Color(0, 0, 255));
		btnOptions.setFont(customFont.deriveFont(Font.BOLD, 35));
		btnOptions.setHorizontalAlignment(SwingConstants.CENTER);
		backGroundLayer.setLayer(btnOptions, 1);
		btnOptions.setBounds(10, 300, 780, 47);
		backGroundLayer.add(btnOptions);

		btnOptions.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				toOptionView();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				btnOptions.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				btnOptions.setForeground(Color.blue);
			}
		});

		JLabel btnExit = new JLabel("EXIT");
		backGroundLayer.setLayer(btnExit, 1);
		btnExit.setFont(customFont.deriveFont(Font.BOLD, 35));
		btnExit.setHorizontalAlignment(SwingConstants.CENTER);
		btnExit.setForeground(Color.blue);
		btnExit.setBounds(10, 379, 780, 47);
		backGroundLayer.add(btnExit);
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				System.exit(0);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				btnExit.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				btnExit.setForeground(Color.blue);
			}
		});

		JLabel btnTitleGame = new JLabel("TIC TAC TOE");
		backGroundLayer.setLayer(btnTitleGame, 1);
		btnTitleGame.setFont(customFont.deriveFont(Font.BOLD | Font.ITALIC, 50));
		btnTitleGame.setHorizontalAlignment(SwingConstants.CENTER);
		btnTitleGame.setForeground(Color.red);
		btnTitleGame.setBounds(0, 131, 800, 69);
		backGroundLayer.add(btnTitleGame);
		
		JLabel lblAuthor = new JLabel("Alpuin Matias");
		backGroundLayer.setLayer(lblAuthor, 1);
		lblAuthor.setFont(customFont.deriveFont(Font.BOLD | Font.ITALIC, 25));
		lblAuthor.setHorizontalAlignment(SwingConstants.CENTER);
		lblAuthor.setForeground(Color.BLUE);
		lblAuthor.setBounds(598, 540, 202, 60);
		backGroundLayer.add(lblAuthor);
	}
}
