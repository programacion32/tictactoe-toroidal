package model;

public class TicTacToe {
	private Table<Player> table;
	private String winner;
	private int turnCounter;

	private boolean turn;
	private boolean gameOver;
	private boolean normalGame;

	private Player player1;
	private Player player2;

	public TicTacToe() {
		this.player1 = new Player("O", "");
		this.player2 = new Player("X", "");
		play();
	}

	public TicTacToe(String player1, String player2) {
		this.player1 = new Player(player1, "");
		this.player2 = new Player(player2, "");
		play();
	}

	public void play() {
		this.table = new Table<Player>(this.normalGame);
		this.turnCounter = 0;
		this.turn = true; // false = O, true = X
		this.winner = "DRAW";
		this.gameOver = false;
	}

	public boolean insertPlay(int pos) {
		if (this.table.getTableRecord(pos) == null && !gameOver) {
			this.turnCounter++;
			if (this.table.addElement(pos, (this.turn ? player1 : player2))) {
				this.winner = "WINNER " + (turn ? player1.getName() : player2.getName());				
				gameOver = true;
			}
			if(this.turnCounter==9) {
				gameOver = true;
			}
			this.turn = !this.turn;
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(this.table);
		s.append("Winner:" + this.winner);
		return s.toString();
	}

	public int getTurnCounter() {
		return this.turnCounter;
	}

	public String getWinner() {
		return this.winner;
	}

	public boolean isGameOver() {
		return this.gameOver;
	}

	public boolean isTurn() {
		return this.turn;
	}

	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public boolean isNormalGame() {
		return this.normalGame;
	}

	public void setNormalGame(boolean normalGame) {
		this.normalGame = normalGame;
	}

	public Player whoPlaysNow() {
		return this.turn ? this.player1 : this.player2;
	}

}
