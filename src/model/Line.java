package model;

import java.util.ArrayList;

public class Line<E> {
	private ArrayList<E> line;
	private int limit;
	private boolean checkEquals;

	public Line(int limit) {
		this.checkEquals = true;
		line = new ArrayList<E>();
		this.limit = limit;
	}

	public boolean addElement(E e) {
		this.line.add(e);
		this.limit--;
		if (limit == 0) {
			checkAllElementEquals();
			return checkEquals;
		}
		return false;
	}


	@Override
	public String toString() {
		String s = "";
		for(E t : line) {
			s += t.toString() + "\t";
		}
		return s;
	}

	public E getLastElement() {
		return this.line.get(line.size() - 1);
	}

	public int getLimit() {
		return limit;
	}
	
	private void checkAllElementEquals() {
		E bk = null;
		for (E t : line) {
			if (bk == null) {
				bk = t;
			} else {
				this.checkEquals &= t.equals(bk);
				bk = t;
			}
		}
	}
	

}
