package model;

import java.util.HashMap;

public class Table<T> {

	private HashMap<Integer, Line<T>> table;
	private HashMap<Integer, T> tableRecordPosition;
	private int solutions;
	private boolean gameOver;

	public Table(boolean normalGame) {
		if (normalGame)
			this.solutions = 8;
		else
			this.solutions = 12;
		buildTable();
	}

	private void buildTable() {
		this.tableRecordPosition = new HashMap<Integer, T>();
		this.table = new HashMap<Integer, Line<T>>();
		for (int i = 0; i < 9; i++) {
			tableRecordPosition.put(i + 1, null);
		}

		for (int i = 0; i < solutions; i++) {
			Line<T> line = new Line<T>(3);
			table.put(i + 1, line);
		}
	}

	public boolean addElement(int pos, T e) {
		if (tableRecordPosition.get(pos) == null) {
			tableRecordPosition.put(pos, e);
			switch (pos) {
			case 1:
				gameOver |= this.table.get(1).addElement(e);
				gameOver |= this.table.get(4).addElement(e);
				gameOver |= this.table.get(7).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(11).addElement(e);
				}
				break;
			case 2:
				gameOver |= this.table.get(1).addElement(e);
				gameOver |= this.table.get(5).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(9).addElement(e);
					gameOver |= this.table.get(10).addElement(e);
				}
				break;
			case 3:
				gameOver |= this.table.get(1).addElement(e);
				gameOver |= this.table.get(6).addElement(e);
				gameOver |= this.table.get(8).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(12).addElement(e);
				}
				break;
			case 4:
				gameOver |= this.table.get(2).addElement(e);
				gameOver |= this.table.get(4).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(10).addElement(e);
					gameOver |= this.table.get(12).addElement(e);
				}
				break;
			case 5:
				gameOver |= this.table.get(2).addElement(e);
				gameOver |= this.table.get(5).addElement(e);
				gameOver |= this.table.get(7).addElement(e);
				gameOver |= this.table.get(8).addElement(e);
				break;
			case 6:
				gameOver |= this.table.get(2).addElement(e);
				gameOver |= this.table.get(6).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(9).addElement(e);
					gameOver |= this.table.get(11).addElement(e);
				}
				break;
			case 7:
				gameOver |= this.table.get(3).addElement(e);
				gameOver |= this.table.get(4).addElement(e);
				gameOver |= this.table.get(8).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(9).addElement(e);
				}
				break;
			case 8:
				gameOver |= this.table.get(3).addElement(e);
				gameOver |= this.table.get(5).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(11).addElement(e);
					gameOver |= this.table.get(12).addElement(e);
				}
				break;
			case 9:
				gameOver |= this.table.get(3).addElement(e);
				gameOver |= this.table.get(6).addElement(e);
				gameOver |= this.table.get(7).addElement(e);
				if (solutions > 8) {
					gameOver |= this.table.get(10).addElement(e);
				}
				break;
			}
		}
		return gameOver;
	}

	public T getTableRecord(int pos) {
		return tableRecordPosition.get(pos);
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("Table:\n");
		s.append("--------------------\n");
		s.append(this.tableRecordPosition.get(1));
		s.append("\t");
		s.append(this.tableRecordPosition.get(2));
		s.append("\t");
		s.append(this.tableRecordPosition.get(3));
		s.append("\n");
		s.append(this.tableRecordPosition.get(4));
		s.append("\t");
		s.append(this.tableRecordPosition.get(5));
		s.append("\t");
		s.append(this.tableRecordPosition.get(6));
		s.append("\n");
		s.append(this.tableRecordPosition.get(7));
		s.append("\t");
		s.append(this.tableRecordPosition.get(8));
		s.append("\t");
		s.append(this.tableRecordPosition.get(9));
		s.append("\n--------------------\n");
		return s.toString();
	}

}
