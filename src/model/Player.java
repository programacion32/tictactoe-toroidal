package model;

public class Player {
	private String name;
	private String image;

	public Player(String name, String image) {
		this.name = name;
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public boolean equals(Object obj) {
		Player player = this;
		Player other = (Player) obj;

		if (other == null) {
			return false;
		}

		if (player.getName().equals(other.getName()) && player.getImage().equals(other.getImage())) {
			return true;
		}

		return false;
	}
	
	@Override
	public String toString() {		
		return this.name;
	}
}
